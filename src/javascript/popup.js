import Pickr from '@simonwep/pickr';

const pickr = Pickr.create({
  el: '.color-picker',
  theme: 'nano', // or 'monolith', or 'classic'

  swatches: [
    'rgba(244, 67, 54, 1)',
    'rgba(233, 30, 99, 0.95)',
    'rgba(156, 39, 176, 0.9)',
    'rgba(103, 58, 183, 0.85)',
    'rgba(63, 81, 181, 0.8)',
    'rgba(33, 150, 243, 0.75)',
    'rgba(3, 169, 244, 0.7)',
    'rgba(0, 188, 212, 0.7)',
    'rgba(0, 150, 136, 0.75)',
    'rgba(76, 175, 80, 0.8)',
    'rgba(139, 195, 74, 0.85)',
    'rgba(205, 220, 57, 0.9)',
    'rgba(255, 235, 59, 0.95)',
    'rgba(255, 193, 7, 1)',
    'rgba(0, 0, 0, 0.25)'
  ],
  defaultRepresentation: 'RGBA',
  default: 'rgba(0, 0, 0, 0.5)',
  components: {
    // Main components
    preview: false,
    opacity: true,
    hue: true,
    // Input / output Options
    interaction: {
      hex: false,
      rgba: false,
      hsla: false,
      hsva: false,
      cmyk: false,
      input: true,
      clear: true,
      save: true
    }
  }
});

pickr.show();
let overlay;

pickr.on('change', data => {
  overlay = pickr.getColor().toRGBA().toString(1);
  chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
    var activeTab = tabs[0];
    chrome.tabs.sendMessage(activeTab.id, {
      msg: 'overlay',
      overlay: overlay
    });
  });
}).on('save', data => {
  chrome.tabs.query({ active: true, currentWindow: true }, function (tabs) {
    chrome.storage.sync.set({ overlay: overlay });
    window.close();
  });
}).on('clear', data => {
  chrome.storage.sync.clear();
  chrome.tabs.query({ currentWindow: true, active: true }, function (tabs) {
    var activeTab = tabs[0];
    chrome.tabs.sendMessage(activeTab.id, {
      msg: 'overlay',
      overlay: 'rgba(0, 0, 0, 0)'
    });
    window.close();
  });
});
