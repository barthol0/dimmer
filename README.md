# Overview
Dimmer is a simple browser extension that lets you dim browser windows

![Demo](https://gitlab.com/vipenm/dimmer/-/raw/master/dimmer.gif)

# Setup
First fork the repository then clone it.

Once downloaded, open it up and run ```npm install && npm run build```

# Usage
Open Chrome (or any other chromium based browser that lets you install chrome extensions) and type ```chrome://extensions``` in the address bar (this might be different depending on browser).

On the extensions page, turn on 'Developer mode' in the top right. Now you should see more options, click 'Load unpacked' and select the root directory of the cloned repo. The extension should now be enabled.

The Dimmer icon should now be visible in the extensions bar of the browser - Enjoy :)

# Debugging
If anything goes wrong, console errors may show up in 1 of 4 places.

First go to ```chrome://extensions```, if you see a button that reads 'Errors' next to Remove, click that and check the error.

If that doesn't resolve your issue, on the extensions page, click 'Inspect views background page (Inactive)' and check console. If there is an error here, the issue is probably in ```background.js```.

The current tab you're in might show an error so open developer console and check for errors, if there are, then the problem is in ```script.js```.

Finally, click the Dimmer icon in the extension toolbar, right click the colour swatch and inspect, if the error is here, check ```popup.js```.

# Docs

For further reading, visit [Google Docs](https://developer.chrome.com/extensions/devguide)

For more information on the colour picker: [Pickr](https://github.com/Simonwep/pickr)